import React, { useCallback, useRef, useState } from 'react';
import styles from './CreateCourse.module.css';
import { mockedAuthorsList, mockedCoursesList } from '../../helpers/constants';
import AuthorItem from './components/AuthorItem/AuthorItem';
import getCurrentData from '../../helpers/getCurrentData';
import { v4 as uuidv4 } from 'uuid';
import Input from '../../common/Input/Input';
import { getCourseDuration } from '../../helpers';
import Button from '../../common/Button/Button';
import { Link } from 'react-router-dom';
import Header from '../Header/Header';

const CreateCourse = () => {
	const [errors, setErrors] = useState({
		title: '',
		description: '',
		duration: '',
	});
	const [hours, setHours] = useState('00:00 hours');

	const [courseAuthors, setCourseAuthors] = useState([]);

	const [copyOfMockedAuthorsList, setCopyOfMockedAuthorsList] =
		useState(mockedAuthorsList);

	const inputRef = useRef(null);

	const handleSubmitForm = useCallback(
		(e) => {
			e.preventDefault();

			const title = e.currentTarget.elements.title.value;
			const description = e.currentTarget.elements.description.value;
			const duration = e.currentTarget.elements.duration.value;

			const titleError = title.length < 2 ? 'Title is required.' : '';
			const descriptionError =
				description.length < 2 ? 'Description is required.' : '';
			const durationError = duration.length < 2 ? 'Duration is required.' : '';

			if (!titleError && !descriptionError && !durationError) {
				const course = {
					id: uuidv4(),
					title,
					description,
					duration,
					authors: courseAuthors.map((el) => el.id),
					creationDate: getCurrentData(new Date()),
				};
				courseAuthors.map((el) => {
					!mockedAuthorsList.find((element) => element.id === el.id) &&
						mockedAuthorsList.push(el);
				});
				mockedCoursesList.push(course);
				console.log(mockedCoursesList);
			}
			setErrors({
				title: titleError,
				description: descriptionError,
				duration: durationError,
			});
		},
		[courseAuthors]
	);

	const handleDurationChange = (e) => {
		const durationValue = e.currentTarget.value;
		const courseDuration = getCourseDuration(+durationValue);
		setHours(courseDuration);
	};

	const handleCreateAuthor = () => {
		const newAuthor = {
			id: uuidv4(),
			name: inputRef.current.value,
		};
		setCopyOfMockedAuthorsList((prevState) => [...prevState, newAuthor]);
	};

	return (
		<>
			<Header showButton={true} />
			<main className='main'>
				<h3>Course Edit/Create Page</h3>
				<form className={styles.form} onSubmit={handleSubmitForm}>
					<h4>Main Info</h4>
					<Input
						labelText='Title'
						name='title'
						errorText={errors.title}
						inputClassName={errors.title.length && 'errorBorder'}
					/>
					<Input
						labelText='Description'
						name='description'
						errorText={errors.description}
						element='textarea'
						inputClassName={errors.description.length && 'errorBorder'}
					/>
					<Input
						onChange={handleDurationChange}
						labelText='Duration'
						name='duration'
						errorText={errors.duration}
						type='number'
						inputClassName={errors.duration.length && 'errorBorder'}
						header='Duration'
						rightElement={hours}
					/>
					<section className={styles.lastSection}>
						<Input
							header='Authors'
							labelText='Author Name'
							rightElement={
								<Button
									buttonText='CREATE AUTHOR'
									onClick={handleCreateAuthor}
									type='button'
								/>
							}
							name='author'
							inputRef={inputRef}
						/>
						<article className={styles.article}>
							<h4>Course Authors</h4>
							<ul>
								{courseAuthors.length
									? courseAuthors.map((el) => <li>{el.name}</li>)
									: 'Authors list is empty'}
							</ul>
						</article>
						<AuthorItem
							copyOfMockedAuthorsList={copyOfMockedAuthorsList}
							setCourseAuthors={setCourseAuthors}
							courseAuthors={courseAuthors}
						/>
					</section>
					<Button buttonText='CREATE COURSE' />
				</form>
				<Link to={'/courses'}>
					<Button buttonText='Cancel' />
				</Link>
			</main>
		</>
	);
};

export default CreateCourse;
