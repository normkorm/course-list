import React from 'react';
import Button from '../../../../common/Button/Button';
import styles from './AuthorItem.module.css';

const SvgAdd = (
	<svg
		width='9'
		height='9'
		viewBox='0 0 9 9'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M1.25 4.5H7.75M4.5 7.75V1.25'
			stroke='#333E48'
			stroke-width='1.5'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);

const SvgDelete = (
	<svg
		width='13'
		height='13'
		viewBox='0 0 13 13'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M11.375 3.23917C9.57125 3.06042 7.75667 2.96833 5.9475 2.96833C4.875 2.96833 3.8025 3.0225 2.73 3.13083L1.625 3.23917M4.60417 2.69208L4.72333 1.9825C4.81 1.46792 4.875 1.08333 5.79042 1.08333H7.20958C8.125 1.08333 8.19542 1.48958 8.27667 1.98792L8.39583 2.69208M10.2104 4.95083L9.85833 10.4054C9.79875 11.2558 9.75 11.9167 8.23875 11.9167H4.76125C3.25 11.9167 3.20125 11.2558 3.14167 10.4054L2.78958 4.95083M5.59542 8.9375H7.39917M5.14583 6.77083H7.85417'
			stroke='#333E48'
			stroke-width='1.5'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);
const AuthorItem = (props) => {
	const handleDeleteAutor = (idToRemove) => {
		props.setCourseAuthors((prevState) =>
			prevState.filter((item) => item.id !== idToRemove)
		);
	};

	const handleAddAuthor = (el) => {
		const isAuthorExists = props.courseAuthors.some(
			(item) => item.id === el.id
		);

		if (!isAuthorExists) {
			props.setCourseAuthors((prevState) => [...prevState, el]);
		}
	};

	return (
		<>
			<ul>
				<h4>Authors List</h4>
				{props.copyOfMockedAuthorsList.map((el) => (
					<li>
						{el.name}
						<Button
							className={styles.button}
							type='button'
							icon={SvgAdd}
							onClick={() => handleAddAuthor(el)}
						/>
						<Button
							className={styles.button}
							type='button'
							icon={SvgDelete}
							onClick={() => handleDeleteAutor(el.id)}
						/>
					</li>
				))}
			</ul>
		</>
	);
};

export default AuthorItem;
