import React from 'react';
import {
	formatCreationDate,
	getCourseByID,
	getCourseDuration,
	getFullNameByID,
} from '../../helpers';
import Button from '../../common/Button/Button';
import styles from './CourseInfo.module.css';
import { mockedCoursesList } from '../../helpers/constants';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import Header from '../Header/Header';

const CourseInfo = () => {
	const { courseId } = useParams();
	const course = getCourseByID(courseId, mockedCoursesList);

	if (course === 'error') {
		return <p>Error: Course not found</p>;
	}

	return (
		<>
			<Header showButton={true} />
			<main className={`${styles.main} main`}>
				<h3>{course.title}</h3>
				<article className={styles.article}>
					<div>
						<h4>Description:</h4>
						<p>{course.description}</p>
					</div>
					<div className={styles.courseInfo}>
						<b>ID:</b>
						<p>{course.id}</p>
						<b>Duration:</b>
						<p>{getCourseDuration(course.duration)}</p>
						<b>Created:</b>
						<p>{formatCreationDate(course.creationDate)}</p>
						<b>Authors:</b>
						<p>{getFullNameByID(course.authors)}</p>
					</div>
				</article>
				<Link to={'/courses'}>
					<Button buttonText='BACK' />
				</Link>
			</main>
		</>
	);
};

export default CourseInfo;
