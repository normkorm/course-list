import React, { useEffect, useState, useCallback } from 'react';
import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import styles from './Header.module.css';
import { Link } from 'react-router-dom';

const Header = ({ showButton }) => {
	const [name, setName] = useState('');
	const status = localStorage.getItem('token');

	const fetchName = useCallback(async () => {
		if (!status) return;

		try {
			const response = await fetch('http://localhost:4000/users/me', {
				method: 'GET',
				headers: {
					accept: '*/*',
					Authorization: status,
				},
			});
			if (!response.ok) {
				console.error('Network response was not ok');
				return;
			}
			const data = await response.json();
			setName(data.result.name);
		} catch (error) {
			console.error('Error fetching the name:', error);
		}
	}, [status]);

	useEffect(() => {
		fetchName();
	}, [status, fetchName]);

	const handleLogout = () => {
		localStorage.removeItem('token');
	};

	return (
		<header className={styles.header}>
			<Logo />
			{showButton && (
				<div className={styles.usernameAndButtonContainer}>
					{status && <p>{name}</p>}
					<Link to={'/login'}>
						<Button
							buttonText={status ? 'LOGOUT' : 'LOGIN'}
							onClick={handleLogout}
						/>
					</Link>
				</div>
			)}
		</header>
	);
};

export default Header;
