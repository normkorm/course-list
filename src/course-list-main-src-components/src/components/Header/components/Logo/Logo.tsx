import React from 'react';
import styles from './Logo.module.css';
import { Link } from 'react-router-dom';
export const Logo = () => (
	<Link to={'/courses'}>
		<img src='/images/logo.jpg' className={styles.logo} alt='logotype' />
	</Link>
);

export default Logo;
