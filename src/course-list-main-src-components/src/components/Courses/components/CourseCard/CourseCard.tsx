import React from 'react';
import {
	formatCreationDate,
	getCourseDuration,
	getFullNameByID,
} from '../../../../helpers';
import Button from '../../../../common/Button/Button';
import styles from './CourseCard.module.css';
import { Link } from 'react-router-dom';

const CourseCard = ({ course }) => {
	return (
		<section className={styles.section}>
			<h2>{course.title}</h2>
			<div className={styles.div}>
				<p>{course.description}</p>
				<article className={styles.article}>
					<p>
						<b>Authors: </b>
						{getFullNameByID(course.authors)}
					</p>
					<p>
						<b>Duration: </b>
						{getCourseDuration(course.duration)}
					</p>
					<p>
						<b>Created: </b>
						{formatCreationDate(course.creationDate)}
					</p>
					<Link to={`/courses/${course.id}`}>
						<Button buttonText='SHOW COURSE' />
					</Link>
				</article>
			</div>
		</section>
	);
};

export default CourseCard;
