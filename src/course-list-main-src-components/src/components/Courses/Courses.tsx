import React from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import { mockedCoursesList } from '../../helpers/constants';
import Header from '../Header/Header';
import Button from '../../common/Button/Button';
import styles from './Courses.module.css';
import { Link } from 'react-router-dom';
const Courses = () => {
	return (
		<>
			<Header showButton={true} />
			<main className={`main ${styles.main}`}>
				<Link to={'/courses/add'}>
					<Button buttonText='ADD NEW COURSE' />
				</Link>
				{mockedCoursesList.map((course) => (
					<CourseCard key={course.id} course={course} />
				))}
			</main>
		</>
	);
};

export default Courses;
