import React from 'react';
import Button from '../../common/Button/Button';
import styles from './EmptyCourseList.module.css';
const EmptyCourseList = () => {
	return (
		<main className={`${styles.emptyContainer} main`}>
			<article className={styles.empty}>
				<h2>Your List Is Empty</h2>
				<p>Please use ’Add New Course’ button to add your first course</p>
				<Button buttonText='Add new course' onClick={() => alert('button')} />
			</article>
		</main>
	);
};

export default EmptyCourseList;
