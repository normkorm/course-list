import React from 'react';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import styles from './Registration.module.css';
import { Link, useNavigate } from 'react-router-dom';
import Header from '../Header/Header';
import { Form, useForm } from 'react-hook-form';
interface IFormInput {
	name: string;
	email: string;
	password: string;
}
const Registration = () => {
	const {
		register,
		control,
		formState: { errors, isSubmitting },
	} = useForm<IFormInput>({
		criteriaMode: 'all',
	});
	const navigate = useNavigate();

	return (
		<>
			<Header showButton={false} />
			<main className='main'>
				<h3 className={styles.title}>Registration</h3>
				<Form
					control={control}
					className={styles.form}
					onSuccess={() => {
						navigate('/login');
					}}
					action='http://localhost:4000/register'
					headers={{
						'Content-Type': 'application/json',
					}}
				>
					<Input
						labelText='Name'
						rest={{
							...register('name', {
								required: { value: true, message: 'Name is required' },
								pattern: {
									value: /^[A-Za-z]+$/i,
									message: 'Alphabetical characters only',
								},
								minLength: {
									value: 2,
									message: 'Name must contain more than two letters',
								},
							}),
						}}
						errorText={errors?.name?.message}
						inputClassName={errors?.name?.message && 'errorBorder'}
					/>
					<Input
						labelText='Email'
						rest={{
							...register('email', {
								required: { value: true, message: 'Email is required' },
								pattern: {
									value: /.+@[^@]+\.[^@]{2,}$/,
									message: 'Please enter a valid email address',
								},
							}),
						}}
						errorText={errors?.email?.message}
						inputClassName={errors?.email?.message && 'errorBorder'}
					/>
					<Input
						labelText='Password'
						type='password'
						rest={{
							...register('password', {
								required: { value: true, message: 'Password is required' },
								minLength: {
									value: 6,
									message: 'Password must be more than 6 characters',
								},
							}),
						}}
						errorText={errors?.password?.message}
						inputClassName={errors?.password?.message && 'errorBorder'}
					/>
					<Button
						disabled={isSubmitting}
						buttonText={isSubmitting ? 'Loading...' : 'Registration'}
					/>
					<Link to={'/login'}>
						<p className={styles.p}>
							If you have an account you may <b>Login</b>
						</p>
					</Link>
					{errors?.root?.server && (
						<p className='error center'>Form submit failed.</p>
					)}
				</Form>
			</main>
		</>
	);
};

export default Registration;
