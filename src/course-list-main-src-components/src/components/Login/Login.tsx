import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import styles from './Login.module.css';
import React from 'react';
import Header from '../Header/Header';
import { Form, useForm } from 'react-hook-form';
interface IFormInput {
	email: string;
	password: string;
	login: string;
}
const Login = () => {
	const {
		register,
		control,
		formState: { errors, isSubmitting },
	} = useForm<IFormInput>({ criteriaMode: 'all' });
	const navigate = useNavigate();

	const onSuccess = async (response) => {
		const res = await response.json();
		localStorage.setItem('token', res.result);
		navigate('/courses');
	};
	return (
		<>
			<Header showButton={false} />
			<main className='main'>
				<h3 className={styles.title}>Login</h3>
				<Form
					className={styles.form}
					control={control}
					action='http://localhost:4000/login'
					headers={{
						'Content-Type': 'application/json',
					}}
					onSuccess={({ response }) => onSuccess(response)}
				>
					<Input
						labelText='Email'
						rest={{
							...register('email', {
								required: { value: true, message: 'Email is required' },
								pattern: {
									value: /.+@[^@]+\.[^@]{2,}$/,
									message: 'Please enter a valid email address',
								},
							}),
						}}
						errorText={errors?.email?.message}
						inputClassName={errors?.email?.message && 'errorBorder'}
					/>
					<Input
						labelText='Password'
						type='password'
						rest={{
							...register('password', {
								required: { value: true, message: 'Password is required' },
								minLength: {
									value: 6,
									message: 'Password must be more than 6 characters',
								},
							}),
						}}
						errorText={errors?.password?.message}
						inputClassName={errors?.password?.message && 'errorBorder'}
					/>
					<Button
						disabled={isSubmitting}
						buttonText={isSubmitting ? 'Loading...' : 'Login'}
					/>
					<Link to={'/registration'}>
						<p className={styles.p}>
							If you don't have an account you may <b>Registration</b>
						</p>
					</Link>
					{errors?.root?.server && (
						<p className='error center'>error with email or password</p>
					)}
				</Form>
			</main>
		</>
	);
};

export default Login;
