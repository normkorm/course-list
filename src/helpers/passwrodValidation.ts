export const passwordValidation = {
	required: { value: true, message: 'Password is required' },
	minLength: { value: 6, message: 'Password must be more than 6 characters' },
};
