export default function getFullNameByID(
	authorsIdList: string[],
	array: { id: string; name: string }[]
) {
	if (!array || array.length === 0) {
		return 'none';
	}

	return authorsIdList
		.map((id: string) => {
			const author = array.find((user) => user.id === id);
			return author ? author.name : "couldn't find the author";
		})
		.join(', ');
}
