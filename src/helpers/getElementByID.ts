export default function getElementByID(ID, array) {
	if (ID !== null) {
		const index = array.findIndex((element) => element.id === ID);
		return array[index];
	}
	return 'error';
}
