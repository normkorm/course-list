import { createAsyncThunk } from '@reduxjs/toolkit';

export const getUserName = createAsyncThunk('getUserName', async () => {
	try {
		const token = sessionStorage.getItem('token');

		if (!token) {
			throw new Error('Token is missing or expired');
		}

		const response = await fetch('http://localhost:4000/users/me', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			},
		});
		const result = await response.json();
		return result.result;
	} catch (error) {
		console.error('Error fetching username:', error);
		throw error;
	}
});
