import { createAsyncThunk } from '@reduxjs/toolkit';

export const addAuthorByName = createAsyncThunk(
	'authors/addAuthor',
	async (name: string, { rejectWithValue }) => {
		try {
			const token = sessionStorage.getItem('token');

			if (!token) {
				throw new Error('Token not found in sessionStorage');
			}

			const response = await fetch(`http://localhost:4000/authors/add`, {
				method: 'POST', // Исправлен метод запроса на POST
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
				body: JSON.stringify({ name }),
			});

			if (!response.ok) {
				throw new Error('Failed to add author');
			}

			return response.json();
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);
