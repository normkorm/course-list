import { createAsyncThunk } from '@reduxjs/toolkit';

export const addCourse = createAsyncThunk(
	'course/addCourse',
	async (course: object, { rejectWithValue }) => {
		try {
			const token = sessionStorage.getItem('token');

			if (!token) {
				throw new Error('Token not found in sessionStorage');
			}

			const response = await fetch(`http://localhost:4000/courses/add`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
				body: JSON.stringify(course),
			});

			if (!response.ok) {
				throw new Error('Failed to add author');
			}

			return response.json();
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);
