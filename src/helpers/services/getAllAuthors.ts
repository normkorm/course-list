import { createAsyncThunk } from '@reduxjs/toolkit';

export const getAllAuthors = createAsyncThunk('getAllAuthors', async () => {
	try {
		const response = await fetch('http://localhost:4000/authors/all');
		const result = await response.json();
		return result.result;
	} catch (error) {
		console.error('Error fetching authors:', error);
		throw error;
	}
});
