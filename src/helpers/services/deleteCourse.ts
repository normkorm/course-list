import { createAsyncThunk } from '@reduxjs/toolkit';

export const deleteCourse = createAsyncThunk(
	'course/deleteCourse',
	async (id: string, { rejectWithValue }) => {
		try {
			const token = sessionStorage.getItem('token');

			if (!token) {
				throw new Error('Token not found in sessionStorage');
			}

			const response = await fetch(`http://localhost:4000/courses/${id}`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			});

			if (!response.ok) {
				throw new Error('Failed to delete course');
			}

			return id;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);
