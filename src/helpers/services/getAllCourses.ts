import { createAsyncThunk } from '@reduxjs/toolkit';

export const getAllCourses = createAsyncThunk('getCourses', async () => {
	try {
		const response = await fetch('http://localhost:4000/courses/all');
		const result = await response.json();
		return result.result;
	} catch (error) {
		console.error('Error fetching courses:', error);
		throw error;
	}
});
