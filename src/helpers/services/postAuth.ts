import { createAsyncThunk } from '@reduxjs/toolkit';

export const postAuth = createAsyncThunk(
	'auth/postAuth',
	async (data: object) => {
		try {
			const response = await fetch('http://localhost:4000/login', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json; charset=UTF-8',
				},
				body: JSON.stringify(data),
			});

			if (!response.ok) {
				throw new Error(
					'An error occurred while executing the request:' + response.status
				);
			}
			const result = await response.json();
			return result;
		} catch (error) {
			console.error('', error);
			throw error;
		}
	}
);
