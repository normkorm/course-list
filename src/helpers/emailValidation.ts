export const emailValidation = {
	required: 'Email is required',
	pattern: {
		value: /.+@[^@]+\.[^@]{2,}$/,
		message: 'Please enter a valid email address',
	},
};
