export default function formatCreationDate(date: string) {
	return date.split('/').join('.');
}
