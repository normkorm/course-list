export default function getCourseDuration(minutes: number): string {
	let hh;
	minutes = Number(minutes);
	if (isNaN(minutes)) {
		return '00:00 hours';
	}
	if (minutes < 0) {
		return '00:00 hours';
	}
	if (minutes > 365 * 24 * 60) {
		return '00:00 hours';
	}
	if (Math.floor(minutes / 60) < 10) {
		hh = `0${Math.floor(minutes / 60)}`;
	}
	if (Math.floor(minutes / 60) >= 10) {
		hh = Math.floor(minutes / 60);
	}
	const mm = minutes % 60 < 10 ? `0${minutes % 60}` : minutes % 60;
	return `${hh}:${mm}${hh === 1 ? 'hour' : ' hours'}`;
}
