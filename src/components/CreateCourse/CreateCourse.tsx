import React, { useEffect, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import getCourseDuration from '../../helpers/getCourseDuration';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { getAllCourses } from '../../helpers/services/getAllCourses';
import { getAllAuthors } from '../../helpers/services/getAllAuthors';
import { addCourse } from '../../helpers/services/addCourse';
import { selectAuthors } from '../../redux';
import styles from './CreateCourse.module.scss';
import Header from '../Header/Header';
import Authors from './Authors';
import { Link } from 'react-router-dom';
interface IFormInput {
	title?: string;
	description?: string;
	duration?: number;
	authorName?: string;
	submittingAuthors?: object[];
}

const schema = yup.object().shape({
	title: yup.string().required().min(2).max(20),
	description: yup.string().required().min(2).max(100),
	duration: yup
		.number()
		.required()
		.min(30)
		.max(365 * 24 * 60)
		.transform((value) => (Number.isNaN(value) ? null : value)),
});

const CreateCourse = () => {
	const dispatch = useAppDispatch();
	const [submittingAuthors, setSubmittingAuthors] = useState([]);
	const [error, setError] = useState('');

	useEffect(() => {
		dispatch(getAllCourses());
		dispatch(getAllAuthors());
	}, [dispatch]);

	const {
		register,
		handleSubmit,
		formState: { errors },
		watch,
	} = useForm<IFormInput>({
		resolver: yupResolver(schema),
	});
	const watchDuration = getCourseDuration(watch('duration'));

	const onSubmit: SubmitHandler<IFormInput> = (data) => {
		const idFromSubmittingAuthors = submittingAuthors.map((elem) => elem.id);
		dispatch(addCourse({ ...data, authors: idFromSubmittingAuthors }));
	};

	return (
		<>
			<Header showButton={true} />
			<main className={styles.main}>
				<h2>Course Edit/Create Page</h2>
				<form onSubmit={handleSubmit(onSubmit)}>
					<h3>Main Info</h3>
					<label htmlFor='title'>
						<b>Title</b>
						<input
							{...register('title')}
							id='title'
							autoComplete='off'
							className={errors.title?.message && 'errorBorder'}
						/>
						<p className={errors.title?.message && 'errorText'}>
							{errors.title?.message}
						</p>
					</label>

					<label htmlFor='description'>
						<b>Description</b>
						<textarea
							{...register('description')}
							id='description'
							autoComplete='off'
							className={errors.description?.message && 'errorBorder'}
						/>
						<p className={errors.description?.message && 'errorText'}>
							{errors.description?.message}
						</p>
					</label>

					<h3>Duration</h3>
					<label>
						<b>Duration</b>
						<div>
							<input
								{...register('duration')}
								name='duration'
								id='duration'
								autoComplete='off'
								className={errors.duration?.message && 'errorBorder'}
							/>
							<p>{watchDuration}</p>
						</div>
						<p className={errors.duration?.message && 'errorText'}>
							{errors.duration?.message}
						</p>
					</label>
					<Authors
						setSubmittingAuthors={setSubmittingAuthors}
						submittingAuthors={submittingAuthors}
						setError={setError}
					/>
					<button>submit</button>
				</form>
				<Link to={'/courses'}>
					<button>Cancel</button>
				</Link>
			</main>
		</>
	);
};

export default CreateCourse;
