import React, { useRef } from 'react';
import { addAuthorByName } from '../../helpers/services/addAuthorByName';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { selectAuthors } from '../../redux';
import { deleteAuthorById } from '../../helpers/services/deleteAuthor';
import styles from './Authors.module.scss';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Authors = ({ submittingAuthors, setSubmittingAuthors, setError }) => {
	const inputRef = useRef(null);
	const dispatch = useAppDispatch();
	const authors = useAppSelector(selectAuthors);
	const handleAddAuthorToServer = (name) => {
		dispatch(addAuthorByName(name));
	};

	const handleDeleteAuthorFromServer = (id: string) => {
		if (authors.length === 1) {
			setError('it is impossible to remove all authors');
			return;
		}
		dispatch(deleteAuthorById(id));
		handleDeleteFromSubmittingAuthors(id);
	};

	const handleAddToSubmittingAuthors = (object) => {
		if (!submittingAuthors.some((author) => author.id === object.id)) {
			setSubmittingAuthors((prevAuthors) => [...prevAuthors, object]);
		}
	};

	const handleDeleteFromSubmittingAuthors = (id: string) => {
		const filteredArray = submittingAuthors.filter((elem) => elem.id !== id);
		setSubmittingAuthors(filteredArray);
	};

	return (
		<section className={styles.section}>
			<section>
				<h3>Authors</h3>
				<label htmlFor='AuthorName'>
					<b>AuthorName</b>
				</label>
				<div>
					<input
						ref={inputRef}
						autoComplete='off'
						id='AuthorName'
						name='AuthorName'
					/>
					<button
						type='button'
						onClick={() => handleAddAuthorToServer(inputRef.current.value)}
					>
						CREATE AUTHOR
					</button>
				</div>
			</section>
			<ul>
				<h3>Course Authors</h3>
				{submittingAuthors.length ? (
					submittingAuthors.map((elem) => (
						<div>
							<button
								type='button'
								onClick={() => handleDeleteFromSubmittingAuthors(elem.id)}
							>
								<FontAwesomeIcon icon={faTrash} />
							</button>
							<p>{elem.name}</p>
						</div>
					))
				) : (
					<p>Course list is empty</p>
				)}
			</ul>
			<ul>
				<h4>Authors List</h4>
				{authors.map((elem) => (
					<div>
						<p>{elem.name}</p>
						<button
							type='button'
							onClick={() => handleAddToSubmittingAuthors(elem)}
						>
							+
						</button>
						<button
							type='button'
							onClick={() => handleDeleteAuthorFromServer(elem.id)}
						>
							<FontAwesomeIcon icon={faTrash} />
						</button>
					</div>
				))}
			</ul>
		</section>
	);
};

export default Authors;
