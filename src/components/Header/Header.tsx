import React, { useEffect } from 'react';
import Logo from './components/Logo/Logo';
import styles from './Header.module.scss';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { getUserName } from '../../helpers/services/getUserName';

const Header = ({ showButton }) => {
	const dispatch = useAppDispatch();
	const user = useAppSelector((state) => state.user.user);

	useEffect(() => {
		dispatch(getUserName());
	}, [dispatch]);

	const logout = () => {
		if (user.name) {
			sessionStorage.removeItem('token');
		}
	};

	return (
		<header className={styles.header}>
			<Logo />
			{showButton && (
				<div>
					{user.name && <span>{user.name}</span>}
					<Link to={'/login'}>
						<button onClick={logout}>{user.isAuth ? 'LOGOUT' : 'LOGIN'}</button>
					</Link>
				</div>
			)}
		</header>
	);
};

export default Header;
