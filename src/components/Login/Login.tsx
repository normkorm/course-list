import { Link, useNavigate } from 'react-router-dom';
import React from 'react';
import Header from '../Header/Header';
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { postAuth } from '../../helpers/services/postAuth';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import styles from './Login.module.scss';
interface IFormInput {
	email?: string;
	password?: string;
}

const schema = yup.object().shape({
	email: yup.string().required(),
	password: yup.string().required(),
});

const Login = () => {
	const navigate = useNavigate();
	const dispatch = useAppDispatch();
	const user = useAppSelector((state) => state.user);

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<IFormInput>({
		resolver: yupResolver(schema),
	});
	const onSubmit: SubmitHandler<IFormInput> = (data) => {
		dispatch(postAuth(data)).then((res) => {
			if (res.payload) {
				sessionStorage.setItem('token', res.payload.result.split(' ')[1]);
				navigate('/courses');
			}
		});
	};

	return (
		<>
			<Header showButton={false} />
			<main className={styles.main}>
				<h3>Login</h3>
				<form onSubmit={handleSubmit(onSubmit)}>
					<label htmlFor='email'>
						<b>Email</b>
						<input
							id='email'
							autoComplete='off'
							{...register('email')}
							className={errors.email?.message && 'errorBorder'}
						/>
						<span className={errors.email?.message && 'errorText'}>
							{errors.email?.message}
						</span>
					</label>
					<label htmlFor='password'>
						<b>Password</b>
						<input
							id='password'
							type='password'
							autoComplete='off'
							{...register('password')}
						/>
						<span className={errors.password?.message && 'errorText'}>
							{errors.password?.message}
						</span>
					</label>
					<button type='submit' disabled={user.status === 'pending'}>
						{user.status === 'pending' ? 'Loading...' : 'Login'}
					</button>
					<Link to={'/registration'}>
						<p>
							If you don't have an account you may <b>Registration</b>
						</p>
					</Link>
					{user.error && <p className='errorText'>{user.error}</p>}
				</form>
			</main>
		</>
	);
};

export default Login;
