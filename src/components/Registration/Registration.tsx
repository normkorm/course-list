import { Link, useNavigate } from 'react-router-dom';
import React from 'react';
import Header from '../Header/Header';
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { postRegister } from '../../helpers/services/postRegister';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import styles from './Registration.module.scss';
import { postAuth } from '../../helpers/services/postAuth';
interface IFormInput {
	name?: string;
	email?: string;
	password?: string;
}

const schema = yup.object().shape({
	name: yup.string().required().min(2).max(30),
	email: yup
		.string()
		.required()
		.email()
		.max(100)
		.matches(
			/.+@[^@]+\.[^@]{2,}$/,
			'Could you please provide a valid email address?'
		),
	password: yup.string().required().min(6).max(30),
});

const Registration = () => {
	const dispatch = useAppDispatch();
	const navigate = useNavigate();
	const user = useAppSelector((state) => state.user);

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<IFormInput>({
		resolver: yupResolver(schema),
	});
	const onSubmit: SubmitHandler<IFormInput> = (data) => {
		dispatch(postRegister(data)).then((res) => {
			if (res.payload) {
				dispatch(postAuth(data)).then((res) => {
					if (res.payload) {
						sessionStorage.setItem('token', res.payload.result.split(' ')[1]);
						navigate('/courses');
					}
				});
			}
		});
	};

	return (
		<>
			<Header showButton={false} />
			<main className={styles.main}>
				<h3>Registration</h3>
				<form onSubmit={handleSubmit(onSubmit)}>
					<label htmlFor='name'>
						<b>name</b>
						<input
							id='name'
							autoComplete='off'
							{...register('name')}
							className={errors.name?.message && 'errorBorder'}
						/>
						<span className={errors.name?.message && 'errorText'}>
							{errors.name?.message}
						</span>
					</label>
					<label htmlFor='email'>
						<b>email</b>
						<input
							id='email'
							autoComplete='off'
							{...register('email')}
							className={errors.email?.message && 'errorText'}
						/>
						<span className={errors.email?.message && 'errorText'}>
							{errors.email?.message}
						</span>
					</label>
					<label htmlFor='password'>
						<b>password</b>
						<input
							id='password'
							type='password'
							autoComplete='off'
							{...register('password')}
						/>
						<span className={errors.password?.message && 'errorText'}>
							{errors.password?.message}
						</span>
					</label>
					<button type='submit' disabled={user.status === 'pending'}>
						{user.status === 'pending' ? 'Loading...' : 'Register'}
					</button>
					<Link to={'/login'}>
						<p>
							If you have an account you may <b>Login</b>
						</p>
					</Link>
					{user.error && <p className='errorText'>{user.error}</p>}
				</form>
			</main>
		</>
	);
};

export default Registration;
