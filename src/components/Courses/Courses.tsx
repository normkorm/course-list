import React, { useEffect } from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import Header from '../Header/Header';
import styles from './Courses.module.scss';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { getAllCourses } from '../../helpers/services/getAllCourses';
import { getAllAuthors } from '../../helpers/services/getAllAuthors';
import { selectCourses, selectCoursesStatus } from '../../redux';
import { getUserName } from '../../helpers/services/getUserName';
const Courses = () => {
	const dispatch = useAppDispatch();
	const courses = useAppSelector(selectCourses);
	const coursesStatus = useAppSelector(selectCoursesStatus);

	useEffect(() => {
		dispatch(getAllCourses());
		dispatch(getAllAuthors());
	}, [dispatch]);

	if (coursesStatus === 'pending') {
		return <p>loading...</p>;
	}

	return (
		<>
			<Header showButton={true} />
			<main className={styles.main}>
				<Link to={'/courses/add'}>
					<button>ADD NEW COURSE</button>
				</Link>
				{courses?.map((course) => (
					<CourseCard key={course.id} course={course} />
				))}
			</main>
		</>
	);
};

export default Courses;
