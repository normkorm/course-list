import React from 'react';
import styles from './CourseCard.module.scss';
import { Link } from 'react-router-dom';
import getCourseDuration from '../../../../helpers/getCourseDuration';
import formatCreationDate from '../../../../helpers/formatCreationDate';
import getFullNameByID from '../../../../helpers/getFullNameByID';
import { useAppDispatch, useAppSelector } from '../../../../redux/hooks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPencil } from '@fortawesome/free-solid-svg-icons';
import { deleteCourse } from '../../../../helpers/services/deleteCourse';
import { selectAuthors, selectCourses } from '../../../../redux';

const CourseCard = ({ course }) => {
	const authors = useAppSelector(selectAuthors);
	const courses = useAppSelector(selectCourses);
	const dispatch = useAppDispatch();

	const removeCourse = (id) => {
		if (courses.length === 1) {
			console.log('error: Cannot delete the last course.');
			return;
		}
		dispatch(deleteCourse(id));
	};

	return (
		<div className={styles.card}>
			<h3>{course.title}</h3>
			<div>
				<p>{course.description}</p>
				<article>
					<p>
						<b>Authors: </b>
						{getFullNameByID(course.authors, authors)}
					</p>
					<p>
						<b>Duration: </b>
						{getCourseDuration(course.duration)}
					</p>
					<p>
						<b>Created: </b>
						{formatCreationDate(course.creationDate)}
					</p>
					<Link to={`/courses/${course.id}`}>
						<button>SHOW COURSE</button>
					</Link>
					<button onClick={() => removeCourse(course.id)}>
						<FontAwesomeIcon icon={faTrashAlt} />
					</button>
					<button>
						<FontAwesomeIcon icon={faPencil} />
					</button>
				</article>
			</div>
		</div>
	);
};

export default CourseCard;
