import React, { useEffect, useMemo } from 'react';
import styles from './CourseInfo.module.scss';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import Header from '../Header/Header';
import getElementByID from '../../helpers/getElementByID';
import getCourseDuration from '../../helpers/getCourseDuration';
import formatCreationDate from '../../helpers/formatCreationDate';
import getFullNameByID from '../../helpers/getFullNameByID';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { getAllCourses } from '../../helpers/services/getAllCourses';
import { getAllAuthors } from '../../helpers/services/getAllAuthors';

const CourseInfo = () => {
	const dispatch = useAppDispatch();
	const { courseId } = useParams();

	useEffect(() => {
		dispatch(getAllCourses());
		dispatch(getAllAuthors());
	}, [dispatch]);

	const { courses, authors, statusCourses, statusAuthors } = useAppSelector(
		(state) => ({
			courses: state.courses.courses,
			authors: state.authors.authors,
			statusCourses: state.courses.status,
			statusAuthors: state.authors.status,
		})
	);

	const course = useMemo(
		() => getElementByID(courseId, courses),
		[courseId, courses]
	);

	if (!course || statusCourses === 'pending' || statusAuthors === 'pending') {
		return <p>Loading...</p>;
	}

	return (
		<>
			<Header showButton={true} />
			<main className={styles.main}>
				<h3>{course.title}</h3>
				<article>
					<div>
						<h4>Description:</h4>
						<p>{course.description}</p>
					</div>
					<div>
						<b>ID:</b>
						<p>{course.id}</p>
						<b>Duration:</b>
						<p>{getCourseDuration(course.duration)}</p>
						<b>Created:</b>
						<p>{formatCreationDate(course.creationDate)}</p>
						<b>Authors:</b>
						<p>{getFullNameByID(course.authors, authors)}</p>
					</div>
				</article>
				<Link to={'/courses'}>
					<button>Back</button>
				</Link>
			</main>
		</>
	);
};

export default CourseInfo;
