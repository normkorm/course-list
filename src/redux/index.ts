import { configureStore } from '@reduxjs/toolkit';
import coursesSlice from './courses';
import authorsSlice from './authors';
import userSlice from './user';

const store = configureStore({
	reducer: {
		courses: coursesSlice,
		authors: authorsSlice,
		user: userSlice,
	},
});
export default store;

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export const selectCourses = (state) => state.courses.courses;
export const selectAuthors = (state) => state.authors.authors;
export const selectCoursesStatus = (state) => state.courses.status;
