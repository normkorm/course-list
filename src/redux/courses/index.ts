import { createSlice } from '@reduxjs/toolkit';
import { getAllCourses } from '../../helpers/services/getAllCourses';
import { addCourse } from '../../helpers/services/addCourse';
import { deleteCourse } from '../../helpers/services/deleteCourse';

const initialState = {
	courses: [],
	status: 'idle',
};

const coursesSlice = createSlice({
	name: 'courses',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getAllCourses.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(getAllCourses.fulfilled, (state, action) => {
				state.status = 'success';
				state.courses = action.payload;
			})
			.addCase(getAllCourses.rejected, (state) => {
				state.status = 'failed';
			})

			.addCase(addCourse.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(addCourse.fulfilled, (state, action) => {
				state.status = 'success';
				state.courses.push(action.payload.result);
			})
			.addCase(addCourse.rejected, (state) => {
				state.status = 'failed';
			})

			.addCase(deleteCourse.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(deleteCourse.fulfilled, (state, action) => {
				state.status = 'success';
				state.courses = state.courses.filter(
					(course) => course.id !== action.payload
				);
			})
			.addCase(deleteCourse.rejected, (state) => {
				state.status = 'failed';
			});
	},
});

export default coursesSlice.reducer;
