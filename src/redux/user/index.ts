import { createSlice } from '@reduxjs/toolkit';
import { postAuth } from '../../helpers/services/postAuth';
import { postRegister } from '../../helpers/services/postRegister';
import { getUserName } from '../../helpers/services/getUserName';

const initialState = {
	user: {
		isAuth: !!sessionStorage.getItem('token'),
		name: null,
		email: null,
	},
	status: 'idle',
	error: null,
};

const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		logoutUser(state) {
			state.user = initialState.user;
			state.status = 'idle';
			state.error = null;
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(postAuth.pending, (state) => {
				state.status = 'pending';
				state.error = null;
			})
			.addCase(postAuth.fulfilled, (state, action) => {
				state.status = 'success';
				state.user = action.payload.user;
				state.user.isAuth = true;
				state.error = null;
			})
			.addCase(postAuth.rejected, (state, action) => {
				state.status = 'idle';
				state.error = action.error.message;
			})

			.addCase(postRegister.pending, (state) => {
				state.status = 'pending';
				state.error = null;
			})
			.addCase(postRegister.fulfilled, (state) => {
				state.status = 'success';
				state.error = null;
			})
			.addCase(postRegister.rejected, (state, action) => {
				state.status = 'idle';
				state.error = action.error.message;
			})

			.addCase(getUserName.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(getUserName.fulfilled, (state, action) => {
				state.status = 'succeeded';
				state.user.name = action.payload.name;
			})
			.addCase(getUserName.rejected, (state) => {
				state.status = 'failed';
				state.user.name = null;
				state.user.isAuth = false;
			});
	},
});

export const { logoutUser } = userSlice.actions;

export default userSlice.reducer;
