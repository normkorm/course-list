import { createSlice } from '@reduxjs/toolkit';
import { getAllAuthors } from '../../helpers/services/getAllAuthors';
import { deleteAuthorById } from '../../helpers/services/deleteAuthor';
import { addAuthorByName } from '../../helpers/services/addAuthorByName';

const initialState = {
	authors: [],
	status: '',
};

const authorsSlice = createSlice({
	name: 'authors',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getAllAuthors.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(getAllAuthors.fulfilled, (state, action) => {
				state.status = 'success';
				state.authors = action.payload;
			})
			.addCase(getAllAuthors.rejected, (state) => {
				state.status = 'failed';
			})

			.addCase(deleteAuthorById.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(deleteAuthorById.fulfilled, (state, action) => {
				state.status = 'success';
				state.authors = state.authors.filter(
					(author) => author.id !== action.payload
				);
			})
			.addCase(addAuthorByName.rejected, (state) => {
				state.status = 'failed';
			})

			.addCase(addAuthorByName.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(addAuthorByName.fulfilled, (state, action) => {
				state.status = 'succeeded';
				state.authors.push(action.payload.result);
			})
			.addCase(deleteAuthorById.rejected, (state) => {
				state.status = 'failed';
			});
	},
});

export default authorsSlice.reducer;
