import React from 'react';
import Courses from './components/Courses/Courses';
import CourseInfo from './components/CourseInfo/CourseInfo';
import CreateCourse from './components/CreateCourse/CreateCourse';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import { Navigate, Route, Routes } from 'react-router';

const App = () => {
	return (
		<Routes>
			<Route path={'/courses'} element={<Courses />} />
			<Route path={'/registration'} element={<Registration />} />
			<Route path={'/login'} element={<Login />} />
			<Route path={'/courses/add'} element={<CreateCourse />} />
			<Route path={'/courses/:courseId'} element={<CourseInfo />} />
			<Route
				path='/'
				element={
					localStorage.getItem('token') ? (
						<Navigate to='/courses' />
					) : (
						<Navigate to='/login' />
					)
				}
			/>
		</Routes>
	);
};

export default App;
